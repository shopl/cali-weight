$(document).ready(function() {
  InitLogoSlider();
})

const InitLogoSlider = function() {
  $("[data-logo-slider]").slick({
    slidesToShow: 4,
    arrows: false,
    dots: true
  });
}